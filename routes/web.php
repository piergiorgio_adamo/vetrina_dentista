<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' , [PublicController::class , "welcome"])->name("homepage");
Route::get('/chi-siamo' , [PublicController::class, "about"])->name("chi-siamo");
Route::get('/servizi' , [PublicController::class , "service"])->name("servizi");

//Rotte FORM DI CONTATTO
Route::get('/contatti' , [PublicController::class , "contacts"])->name("contatti");
Route::post('/contatti/submit' , [PublicController::class , "contactSubmit"])->name("contatti.submit");

// Rotte CHI SIAMO-dettaglio
Route::get("/dettaglio-team/{id}" , [PublicController::class , "dettaglioTeam"  ])->name("dettaglio.team"); 