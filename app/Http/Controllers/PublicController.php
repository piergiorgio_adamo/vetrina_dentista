<?php

namespace App\Http\Controllers;


use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
// use App\Http\Controllers\PublicController;

class PublicController extends Controller
{
    public function welcome(){
        return view("welcome");
    }

    public function contacts(){
        return view('contacts');
    }
 
    public function about(){
        $teams = [  
            ["id" => 1 , "name" =>"Dottore 1" , "curriculum" => "Nel Luglio del 1994 si laurea in Medicina e Chirurgia presso l’Università degli Studi di Milano.

            Nel novembre del 1999, presso lo stesso Ateneo, consegue la Specialità di Chirurgia Maxillo- Facciale.
            
            Dal settembre 2000 a giugno 2006 Dirigente medico di I livello a contratto presso la Casa Di Cura dell’Istituto Stomatologico Italiano di Milano.
            
            Dal 2006 al 2009 Professore a contratto di Chirurgia Orale per la Facoltà di Medicina e Chirurgia Ateneo Vita & Salute H. San Raffaele (Milano) presso il CLID del Dipartimento e Cattedra di Clinica Odontoiatrica.
            
            Dal 2006 al 2016 Direttore Medico del Centro Odontostomatologico dell’Azienda Ospedaliera di Desio e Vimercate.
            
            Dal 2015 al 2016 Direttore Medico del Centro Odontostomatologico del Green Park di Mantova (Ospedale San Pellegrino di C.delle Stiviere).
            
            Dal 2016 Direttore Medico del Reparto Odontostomatologico degli ISTITUTI CLINICI ZUCCHI di Monza SMART DENTAL CLINIC.
            
            Membro del consiglio direttivo C.O.M. (Cenacolo Odontostomatologico Milanese) corresponsabile della divisione di Chirurgia ed Implantologia. E’ membro inoltre della Società Italiana di Chirurgia Maxillo Facciale (SICMF) e della Società Italiana di Chirurgia Orale (SICO).
            
            Esperienza pluriennale in Chirurgia Orale, Chirurgia Ortognatica, Chirurgia Pre-implantare, Chirurgia implantare con particolare attenzione volta alla riabilitazione dei mascellari atrofici.
            
            E’ autore di circa 30 pubblicazioni tra lavori originali, lavori ed abstract da atti congressuali italiani ed esteri.
            
            Svolge inoltre libera professione in Milano presso Studio Dentistico San Paolo" , "istruzione" => "Dottore in Medicina e Chirurgia
            Specializzazione in Chirurgia Maxillo-facciale"],
            ["id" => 2 , "name" =>"Dottore 2" , "curriculum" =>"Dopo avere conseguito la maturità classica, si laurea in Medicina e Chirurgia presso l’Università degli Studi di Milano. Presso lo stesso ateneo si laurea successivamente in Odontoiatria e Protesi Dentaria. Dal settembre 1994 è frequentatore della Clinica Odontoiatrica dell’Università degli Studi di Milano (Fondazione IRCCS – Ospedale Maggiore Policlinico Mangiagalli e Regina Elena).

            Attualmente presso la stessa struttura è Professore a contratto di Discipline Odontostomatologiche 1 e 2, al corso di Laurea di Odontoiatria e Protesi Dentaria, Università degli Studi di Milano.
            
            Esperienza pluriennale in Conservativa, Endodonzia, Protesi Fissa ed Implantare, ha partecipato a numerosi congressi e convegni di aggiornamento. Autore di varie pubblicazioni tra lavori e abstract da atti congressuali, esercita la libera professione in Milano." ,"istruzione" => "Dott. in Medicina e Chirurgia
            Dott. in Odontoiatria e Protesi Dentaria" ],
            ["id" => 3 , "name" =>"Dottore 3" , "curriculum" =>"io sono un lorem" , "istruzione" =>"Odontoiatra
            Specialista in Ortognatodonzia"]


        ];
         return view('about', ["teams" => $teams]);
    }
 
    public function service(){
        return view('service');
   }

   //Gestione dei dati inviati dall'utente
   public function contactSubmit(Request $request){
        $name = $request->name;
        $surname = $request->surname;
        $email = $request->email;
        $phone = $request->phone;
        $message = $request->message;

        $userContact = compact('name','surname','email','phone','message');

        Mail::to($email)->send(new ContactMail($userContact));

        return redirect(route('contatti'))->with('message','Richiesta inviata correttamente');

   }

   //Dettaglio Team
   public function dettaglioTeam($id){ // manca l'id come argomento della funzione

        $teams = [  
            

            ["id" => 1 , "name" =>"Ciccio Pasticcio" , "curriculum" => "Nel Luglio del 1994 si laurea in Medicina e Chirurgia presso l’Università degli Studi di Milano.

            Nel novembre del 1999, presso lo stesso Ateneo, consegue la Specialità di Chirurgia Maxillo- Facciale.
            
            Dal settembre 2000 a giugno 2006 Dirigente medico di I livello a contratto presso la Casa Di Cura dell’Istituto Stomatologico Italiano di Milano.
            
            Dal 2006 al 2009 Professore a contratto di Chirurgia Orale per la Facoltà di Medicina e Chirurgia Ateneo Vita & Salute H. San Raffaele (Milano) presso il CLID del Dipartimento e Cattedra di Clinica Odontoiatrica.
            
            Dal 2006 al 2016 Direttore Medico del Centro Odontostomatologico dell’Azienda Ospedaliera di Desio e Vimercate.
            
            Dal 2015 al 2016 Direttore Medico del Centro Odontostomatologico del Green Park di Mantova (Ospedale San Pellegrino di C.delle Stiviere).
            
            Dal 2016 Direttore Medico del Reparto Odontostomatologico degli ISTITUTI CLINICI ZUCCHI di Monza SMART DENTAL CLINIC.
            
            Membro del consiglio direttivo C.O.M. (Cenacolo Odontostomatologico Milanese) corresponsabile della divisione di Chirurgia ed Implantologia. E’ membro inoltre della Società Italiana di Chirurgia Maxillo Facciale (SICMF) e della Società Italiana di Chirurgia Orale (SICO).
            
            Esperienza pluriennale in Chirurgia Orale, Chirurgia Ortognatica, Chirurgia Pre-implantare, Chirurgia implantare con particolare attenzione volta alla riabilitazione dei mascellari atrofici.
            
            E’ autore di circa 30 pubblicazioni tra lavori originali, lavori ed abstract da atti congressuali italiani ed esteri.
            
            Svolge inoltre libera professione in Milano presso Studio Dentistico San Paolo" , "istruzione" => "Dottore in Medicina e Chirurgia
            Specializzazione in Chirurgia Maxillo-facciale"],
            ["id" => 2 , "name" =>"Ciccio Pastrocchio" , "curriculum" =>"Dopo avere conseguito la maturità classica, si laurea in Medicina e Chirurgia presso l’Università degli Studi di Milano. Presso lo stesso ateneo si laurea successivamente in Odontoiatria e Protesi Dentaria. Dal settembre 1994 è frequentatore della Clinica Odontoiatrica dell’Università degli Studi di Milano (Fondazione IRCCS – Ospedale Maggiore Policlinico Mangiagalli e Regina Elena).

            Attualmente presso la stessa struttura è Professore a contratto di Discipline Odontostomatologiche 1 e 2, al corso di Laurea di Odontoiatria e Protesi Dentaria, Università degli Studi di Milano.
            
            Esperienza pluriennale in Conservativa, Endodonzia, Protesi Fissa ed Implantare, ha partecipato a numerosi congressi e convegni di aggiornamento. Autore di varie pubblicazioni tra lavori e abstract da atti congressuali, esercita la libera professione in Milano." ,"istruzione" => "Dott. in Medicina e Chirurgia
            Dott. in Odontoiatria e Protesi Dentaria" ],
            ["id" => 3 , "name" =>"Ciccio Dentone" , "curriculum" =>"io sono un lorem" , "istruzione" =>"Odontoiatra
            Specialista in Ortognatodonzia"]

        ];
   
            foreach($teams as $team ){
   
                if($id == $team["id"])
   
                return view("dettaglio-team" , ["team" => $team]);

            }

            
        }
    }
    
   
       