(function () {
"use strict";

	//===== ScrollBtn in view service
	
// EVENTI ALLO SCROLL

document.addEventListener("scroll", ()=>{

    let scrolled = window.scrollY;

    if(scrolled >= 150){
        scrollBtn.classList.remove("d-none");
    }else{
        scrollBtn.classList.add("d-none");
    }

})



	//===== Preloader

	window.onload = function () {
		window.setTimeout(fadeout, 500);
	}

	function fadeout() {
		document.querySelector('.preloader').style.opacity = '0';
		document.querySelector('.preloader').style.display = 'none';
	}


	/*=====================================
	Sticky
	======================================= */
	window.onscroll = function () {
		var header_navbar = document.querySelector(".navbar-area");
		var sticky = header_navbar.offsetTop;

		if (window.pageYOffset > sticky) {
			header_navbar.classList.add("sticky");
		} else {
			header_navbar.classList.remove("sticky");
		}



		// show or hide the back-top-top button
		var backToTo = document.querySelector(".scroll-top");
		if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
			backToTo.style.display = "block";
		} else {
			backToTo.style.display = "none";
		}
	};

	// Get the navbar

	//======= tiny slider for slider-active
	tns({
		container: '.slider-active',
		items: 1,
		slideBy: 'page',
		autoplay: true,
		mouseDrag: true,
		gutter: 0,
		nav: true,
		controls: false,
		autoplayButtonOutput: false,
	});

	// for menu scroll 
	var pageLink = document.querySelectorAll('.page-scroll');

	pageLink.forEach(elem => {
		elem.addEventListener('click', e => {
			e.preventDefault();
			document.querySelector(elem.getAttribute('href')).scrollIntoView({
				behavior: 'smooth',
				offsetTop: 1 - 60,
			});
		});
	});

	// section menu active
	function onScroll(event) {
		var sections = document.querySelectorAll('.page-scroll');
		var scrollPos = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;

		for (var i = 0; i < sections.length; i++) {
			var currLink = sections[i];
			var val = currLink.getAttribute('href');
			var refElement = document.querySelector(val);
			var scrollTopMinus = scrollPos + 73;
			if (refElement.offsetTop <= scrollTopMinus && (refElement.offsetTop + refElement.offsetHeight > scrollTopMinus)) {
				document.querySelector('.page-scroll').classList.remove('active');
				currLink.classList.add('active');
			} else {
				currLink.classList.remove('active');
			}
		}
	};

	window.document.addEventListener('scroll', onScroll);


	//===== close navbar-collapse when a  clicked
	let navbarToggler = document.querySelector(".navbar-toggler");
	var navbarCollapse = document.querySelector(".navbar-collapse");

	document.querySelectorAll(".page-scroll").forEach(e =>
		e.addEventListener("click", () => {
			navbarToggler.classList.remove("active");
			navbarCollapse.classList.remove('show')
		})
	);
	navbarToggler.addEventListener('click', function () {
		navbarToggler.classList.toggle("active");
	})




	//======== WOW active
	new WOW().init();



	$(function() {
		// init the validator
		// validator files are included in the download package
		// otherwise download from http://1000hz.github.io/bootstrap-validator
	  
		$("#contact-form").validator();
	  
		// when the form is submitted
		$("#contact-form").on("submit", function(e) {
		  // if the validator does not prevent form submit
		  if (!e.isDefaultPrevented()) {
			var url = "contact.php";
	  
			// FOR CODEPEN DEMO I WILL PROVIDE THE DEMO OUTPUT HERE, download the PHP files from
			// https://bootstrapious.com/p/how-to-build-a-working-bootstrap-contact-form
	  
			var messageAlert = "alert-success";
			var messageText =
			  "Your message was sent, thank you. I will get back to you soon.";
	  
			// let's compose Bootstrap alert box HTML
			var alertBox =
			  '<div class="alert ' +
			  messageAlert +
			  ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
			  messageText +
			  "</div>";
	  
			// If we have messageAlert and messageText
			if (messageAlert && messageText) {
			  // inject the alert to .messages div in our form
			  $("#contact-form").find(".messages").html(alertBox);
			  // empty the form
			  $("#contact-form")[0].reset();
			}
	  
			return false;
		  }
		});
	  });



})();	