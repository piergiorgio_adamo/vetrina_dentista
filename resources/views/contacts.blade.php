<x-layout>
<!-- ========================= header start ========================= -->
<header id="home" class="header">

    <div class="header-wrapper">

        <x-headerTop />

        <x-navbar />

    </div>

    <div>
        @if (session('message'))
            <div class="alert alert-success">
                {{ session('message')}}
            </div>
        @endif
    </div>

    <!-- ========================= contact-section start ========================= -->
    <section id="contact" class="team-section vh-100">
        <div class="shape shape-5">
            <img src="media/shapes/shape-2.svg" alt="">
        </div>
        <div class="shape shape-6">
            <img src="media/shapes/shape-5.svg" alt="">
        </div>
        <div class="container my-1">
            <div class="row">
                <div class="col-lg-12 col-lg-offset-2">
                    <h1 id="titolo-contatti" class="mt-3 mb-15">Inserisci le info per essere ricontattato:</h1>      
                    <form id="contact-form" method="post" action="{{route('contatti.submit')}}" role="form">
                    @csrf
                    <div class="messages"></div>
                    <div class="controls">
                        <div class="row my-2">
                            <div class="col-md-6" data-aos="fade-right">
                                <div class="form-group">
                                    <label for="form_name">Nome*</label>
                                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Inserisci qui il tuo nome *" required="required"    data-error="Campo obbligatorio.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6" data-aos="fade-left">
                                <div class="form-group">
                                    <label for="form_lastname">Cognome*</label>
                                    <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Inserisci qui il tuo cognome *" required="required" data-error="Campo obbligatorio.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-md-6" data-aos="fade-right">
                                <div class="form-group">
                                    <label for="form_email">Email*</label>
                                    <input id="form_email" type="email" name="email" class="form-control" placeholder="Inserisci una e-mail valida *" required="required" data-error="Inserisci una mail valida.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6" data-aos="fade-left">
                                <div class="form-group">
                                    <label for="form_phone">Telefono</label>
                                    <input id="form_phone" type="tel" pattern="[0-9]{3}[0-9]{3}[0-9]{4}" name="phone" class="form-control" placeholder="Inserisci un telefono dove poter essere ricontattato">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_message">Messaggio*</label>
                                    <textarea id="form_message" name="message" class="form-control" placeholder="Inserisci qui la tua richiesta *" rows="6" required="required" data-error="Per favore, indicaci la tua richiesta."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12 my-3">
                                <input type="submit" id="invio-mail" class="btn btn-send" value="Invia richiesta">
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-md-12">
                                <p class="text-muted"><strong>*</strong>Questi campi sono obbligatori</p>
                            </div>
                        </div>
                    </div>
                    </form>
                </div> 
            </div> 
        </div>    
    </section>
    <!-- ========================= contact-section end ========================= -->            
    </header>
    <!-- ========================= header end ========================= -->
</x-layout>