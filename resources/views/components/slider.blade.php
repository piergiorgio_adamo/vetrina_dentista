<section class="slider-section">
    <div class="slider-active">
        <div class="single-slider img-bg" style="background-image:url('media/slider/slider-3.jpg');">
            <div class="container">
                <div class="row">
                    <div class="col-xl-7 col-lg-8 col-md-10">
                        <div class="slider-content">
                            <h1 data-animation="fadeInDown" data-duration="1.5s" data-delay=".5s">Studio Dentistico
                            </h1>
                            <p data-animation="fadeInLeft" data-duration="1.5s" data-delay=".7s">Professionalità e Passione al vostro servizio. Nel centro di Milano.</p>
                            <a href="{{Route('chi-siamo')}}" class="btn theme-btn page-scroll" data-animation="fadeInUp" data-duration="1.5s"
                                data-delay=".9s">Scopri di più</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
