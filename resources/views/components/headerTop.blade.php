<div class="header-top theme-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="header-top-left text-center text-md-left">
                    <ul>
                        <li><a href="#"><i class="lni lni-phone"></i> +39.335.2574879685</a></li>
                        <li><a href="{{route('contatti')}}"><i class="lni lni-envelope"></i> info@dentista.it</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="header-top-right d-none d-md-block">
                    <ul>
                        <li><a href="https://www.facebook.com" target="_blank"><i class="lni lni-facebook-filled"></i></a></li>
                        <li><a href="https://www.twitter.com" target="_blank"><i class="lni lni-twitter-filled"></i></a></li>
                        <li><a href="https://www.linkedin.com" target="_blank""><i class="lni lni-linkedin-original"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>