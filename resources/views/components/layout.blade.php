<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <title>Dentista 1.0</title>
  </head>
  <body>
   
    <x-preloader />

    {{$slot}}

    <!-- Scroll Button -->
    <a id="scrollBtn" class="scroll-button d-flex d-none align-items-center" href="#" >
      <i class="fas fa-arrow-up fa-2x"></i>
    </a>
    <x-footer />

    {{-- Script FontAwesome --}}
    <script src="https://kit.fontawesome.com/3b3f1e11b8.js" crossorigin="anonymous"></script>
    <!-- JavaScript -->
    <script src="{{asset('js/app.js')}}"></script>   
  </body>
</html>