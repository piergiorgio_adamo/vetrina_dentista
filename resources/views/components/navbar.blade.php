<div class="navbar-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-expand-lg">

                    <a class="navbar-brand mt-3" href="/">
                        <img src="/media/logo/logo.png" alt="Logo" class="logo-dent">
                    </a>
                    
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"             data-bs-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"    aria-label="Toggle navigation">
                        <span class="toggler-icon"></span>
                        <span class="toggler-icon"></span>
                        <span class="toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
                        <ul id="nav" class="navbar-nav ms-lg-auto">
                            <li class="nav-item">
                                <a class="page-scroll {{(Route::currentRouteName() == 'homepage') ? 'active' : '' }}" href="{{route('homepage')}}">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="page-scroll {{(Route::currentRouteName() == 'chi-siamo') ? 'active' : '' }}" href="{{route('chi-siamo')}}">Chi siamo</a>
                            </li>
                            <li class="nav-item">
                                <a class="page-scroll {{(Route::currentRouteName() == 'servizi') ? 'active' : '' }}" href="{{route('servizi')}}">Servizi</a>
                            </li>
                            <li class="nav-item">
                                <a class="page-scroll {{(Route::currentRouteName() == 'contatti') ? 'active' : '' }}" href="{{route('contatti')}}">Contatti</a>
                            </li>
                        </ul>
                    </div> <!-- navbar collapse -->
                </nav> <!-- navbar -->
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
</div> <!-- navbar area -->