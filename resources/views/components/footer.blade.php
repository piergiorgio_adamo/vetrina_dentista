<!-- ========================= footer start ========================= -->
<footer class="footer pt-100 img-bg" style="background-image:url('/media/bg/footer-bg.jpg');">
    <div class="container">
        <div class="footer-widget-wrapper">
            <div class="row">
                <div class="col-xl-4 col-lg-5 col-md-6">
                    <div class="footer-widget mb-30">
                        <a href="{{route('homepage')}}" class="logo logo-dent-footer"><img src="/media/logo/logo.png" alt=""></a><span class="text-white">DENTISTA.it</span>
                        <p>Venite a trovarci nel centro dentistico in pieno centro a Milano.</p>
                        <div class="footer-social-links">
                            <ul>
                                <li><a href="https://www.facebook.com" target="_blank"><i class="lni lni-facebook-filled"></i></a></li>
                                <li><a href="https://www.twitter.com" target="_blank""><i class="lni lni-twitter-filled"></i></a></li>
                                <li><a href="https://www.linkedin.com" target="_blank""><i class="lni lni-linkedin-original"></i></a></li>
                                <li><a href="https://www.instagram.com" target="_blank""><i class="lni lni-instagram-original"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-12 d-flex justify-content-lg-center">
                    <div class="footer-widget mb-30">
                        <h4>Link alle pagine</h4>
                        <ul class="footer-links">
                            <li><a href="{{route('homepage')}}">Home</a></li>
                            <li><a href="{{route('chi-siamo')}}">Team</a></li>
                            <li><a href="{{route('servizi')}}">Servizi</a></li>
                            <li><a href="{{route('contatti')}}">Contatti</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-12 col-md-7">
                    <div class="footer-widget mb-30">
                        <h4>Centro Dentistico - Dove siamo</h4>
                        <div class="map-canvas">
                            <!-- <iframe class="map" id="gmap_canvas"
                                src="https://maps.google.com/maps?q=Mission%20District%2C%20San%20Francisco%2C%20CA%2C%20USA&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed"></iframe> -->
                            <iframe class="map" id="gmap_canvas" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2204.9693415785227!2d9.193043680067772!3d45.46540664704938!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4786c6ae5454038f%3A0x5f66a9ed36a9a6c5!2sThe%20Space%20Cinema!5e0!3m2!1sit!2sit!4v1644010253576!5m2!1sit!2sit" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- ========================= footer end ========================= -->
