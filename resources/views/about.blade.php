<x-layout>
    <!-- ========================= header start ========================= -->
    <header id="home" class="header">

        <div class="header-wrapper">

            <x-headerTop />

            <x-navbar />

        </div>

        <!-- ========================= contact-section start ========================= -->
        <section id="contact" class="team-section pb-100">
            <div class="row justify-content-around">
                <h1 class="text-center mt-50 mb-50" id="titolo-about">CHI SIAMO</h1>
                    @foreach ($teams as $team)
                        <div class="col-4 d-flex justify-content-center">
                        <a href="{{route('dettaglio.team' ,$team['id'])  }}">   
                            <x-card                 
                            id="{{$team['id']}}"
                            name="{{$team['name']}}"
                            />
                        </a>
                        </div>
                    @endforeach
            </div>
        </section>
	<!-- ========================= contact-section end ========================= -->
    </header>
<!-- ========================= head er end ========================= -->
</x-layout>