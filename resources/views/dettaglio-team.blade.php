<x-layout>
 
    <div class="header-wrapper">

        <x-headerTop />

        <x-navbar />

    </div>
    
    <div class="container">
        <div class="row my-5">    
            <div class="col-12">
                <h2 id="titolo-dettaglio" class="text-center">{{$team['name']}}</h2>
            </div>
        </div>
        <div class="row justify-content-center my-5">
            <div class="col-8 text-center">    
                <span id="detailsCard">{{$team['curriculum']}}</span>
                <span id="detailsCard">{{$team['istruzione']}}</span>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-12 d-flex">    
                <img class ="img-fluid mx-auto" id="foto-personale" src="https://picsum.photos/300" alt="">
            </div>
        </div>
        <div class="row my-5 justify-content-center">
            <div class="col-2">
                <a href="{{Route('chi-siamo')}}" class="btn theme-btn page-scroll" data-animation="fadeInUp" data-duration="1.5s"                               data-delay=".9s">Torna indietro</a>
            </div>
        </div>
        
    </div>

</x-layout>
