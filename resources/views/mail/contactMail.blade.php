<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <title>Mail di contatto</title>
  </head>
  <body>
    <h1>Buona sera Sig. {{$userContact['surname']}} {{$userContact['name']}} e grazie per averci contattato</h1>
    <h3>Lei ci ha inviato il seguente messaggio: {{$userContact['message']}} </h3>
    <h3>In merito alla sua richiesta verrà ricontattato al più presto dal nostro staff all'indirzzo mail da lei fornitoci o direttamente tramite telefono non appena ne avremo la possibilità.</h3>
  </body>
</html>