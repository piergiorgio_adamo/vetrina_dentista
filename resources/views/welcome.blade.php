<x-layout>
    <!-- ========================= header start ========================= -->
    <header id="home" class="header">
        
        <!-- <div class="header-wrapper"> -->

            <x-headerTop />

            <x-navbar />
        
            <x-slider />
        
        <!-- </div> -->
         
    </header>

    
<!-- ========================= header end ========================= -->
</x-layout>