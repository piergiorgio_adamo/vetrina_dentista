<x-layout>
    <!-- ========================= header start ========================= -->
    <header id="home" class="header">

        <div class="header-wrapper">

            <x-headerTop />

            <x-navbar />

        </div>

    

        <!-- ========================= header end ========================= -->
        
        <section class="container-fluid">

            <!-- RISOLTO IL PROBLEMA DELLA RAW DIAGNOSI CHE VA SOTTO ALLA NAVBAR -->
                <h1 class="text-center mt-75" id="titolo-servizi">I NOSTRI SERVIZI</h1>

                <!-- RAW DIAGNOSI -->
                <div class="row sep-line align-items-center text-center justify-content-evenly mx-auto vh-80">
                    <div class="col-12 d-flex flex-column align-items-center justify-content-center col-lg-6">
                        <img src="https://picsum.photos/300" class="img-fluid d-none d-lg-block img-service  rounded-circle" alt="">
                    </div>
                    <div class="col-12 box-hover d-flex flex-column align-items-center justify-content-center col-lg-5 me-auto  card-servizi shadow">
                        <h4 class="mb-2" >DIAGNOSI</h4>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquam ipsa dolorem culpa similique itaque debitis excepturi minima nemo, ab dolores magnam ipsam, dignissimos dicta perferendis fugit consectetur natus cupiditate laudantium.</p>
                    </div>
                </div>

                <!-- RAW IMPLATOLOGIA -->
                <div class="row sep-line align-items-center text-center justify-content-evenly mx-auto vh-80">
                    <div class="col-8 box-hover d-flex flex-column align-items-center justify-content-center col-lg-4  card-servizi shadow">
                        <h4 class="mb-2" >IMPLANTOLOGIA</h4>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquam ipsa dolorem culpa similique itaque debitis excepturi minima nemo, ab dolores magnam ipsam, dignissimos dicta perferendis fugit consectetur natus cupiditate laudantium.</p>
                    </div>
                    <div class="col-8 d-flex flex-column align-items-center justify-content-center col-lg-4 ">
                        <img src="https://picsum.photos/300" class="img-fluid d-none d-lg-block img-service rounded-circle" alt="">
        
                    </div>
                </div>
        
                <!-- RAW PROTESI -->
                <div class="row sep-line align-items-center text-center justify-content-evenly mx-auto vh-80">
                    <div class="col-8 d-flex flex-column align-items-center justify-content-center col-lg-4 ">
                        <img src="https://picsum.photos/300" class="img-fluid d-none d-lg-block img-service rounded-circle" alt="">
                    </div>
                    <div class="col-8 box-hover d-flex flex-column align-items-center justify-content-center col-lg-4  card-servizi shadow">
                        <h4 class="mb-2" >PROTESI</h4>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquam ipsa dolorem culpa similique itaque debitis excepturi minima nemo, ab dolores magnam ipsam, dignissimos dicta perferendis fugit consectetur natus cupiditate laudantium.</p>

                    </div>
                </div>

                <!-- RAW INVISALING -->
                <div class="row sep-line align-items-center text-center justify-content-evenly mx-auto vh-80">
                
                    <div class="col-8 box-hover d-flex flex-column align-items-center justify-content-center col-lg-4  card-servizi shadow">
                        <h4 class="mb-2" >INVISALING</h4>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquam ipsa dolorem culpa similique itaque debitis excepturi minima nemo, ab dolores magnam ipsam, dignissimos dicta perferendis fugit consectetur natus cupiditate laudantium.</p>

                    </div>
                    <div class="col-8 d-flex flex-column align-items-center justify-content-center col-lg-4 ">
                        <img src="https://picsum.photos/300" class="img-fluid d-none d-lg-block img-service rounded-circle" alt="">

                    </div>
                </div>
                
                <!-- RAW GNATOLOGIA -->
                <div class="row sep-line align-items-center text-center justify-content-evenly mx-auto vh-80">
                    
                    <div class="col-8 d-flex flex-column align-items-center justify-content-center col-lg-4 ">
                        <img src="https://picsum.photos/300" class="img-fluid d-none d-lg-block img-service rounded-circle" alt="">
        
                    </div>
                    <div class="col-8 box-hover d-flex flex-column align-items-center justify-content-center col-lg-4  card-servizi shadow">
                        <h4 class="mb-2" >GNATOLOGIA</h4>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquam ipsa dolorem culpa similique itaque debitis excepturi minima nemo, ab dolores magnam ipsam, dignissimos dicta perferendis fugit consectetur natus cupiditate laudantium.</p>
                    </div>
        
                </div>
                
                <!-- RAW PEDODONZIA -->
                <div class="row sep-line align-items-center text-center justify-content-evenly mx-auto vh-80">
                    
                    <div class="col-8 box-hover d-flex flex-column align-items-center justify-content-center col-lg-4  card-servizi shadow">
                        <h4 class="mb-2" >PEDODONZIA</h4>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquam ipsa dolorem culpa similique itaque debitis excepturi minima nemo, ab dolores magnam ipsam, dignissimos dicta perferendis fugit consectetur natus cupiditate laudantium.</p>
                    </div>
                    <div class="col-8 d-flex flex-column align-items-center justify-content-center col-lg-4 ">
                        <img src="https://picsum.photos/300" class="img-fluid d-none d-lg-block img-service rounded-circle" alt="">
                    </div>

                </div>

                <!-- RAW SBIANCAMENTO -->
                <div class="row sep-line align-items-center text-center justify-content-evenly mx-auto vh-80">
                
                    <div class="col-8 d-flex flex-column align-items-center justify-content-center col-lg-4 ">
                        <img src="https://picsum.photos/300" class="img-fluid d-none d-lg-block img-service rounded-circle" alt="">
                    </div>
                    <div class="col-8 box-hover d-flex flex-column align-items-center justify-content-center col-lg-4  card-servizi shadow">
                        <h4 class="mb-2" >SBIANCAMENTO </h4>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquam ipsa dolorem culpa similique itaque debitis excepturi minima nemo, ab dolores magnam ipsam, dignissimos dicta perferendis fugit consectetur natus cupiditate laudantium.</p>
                    </div>

                </div>

                <!-- RAW TRATTAMENTI LASER -->
                <div class="row sep-line align-items-center text-center justify-content-evenly mx-auto vh-80">

                    <div class="col-8 box-hover d-flex flex-column align-items-center justify-content-center col-lg-4  card-servizi shadow  ">
                        <h4 class="mb-2" >TRATTAMENTI LASER</h4>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquam ipsa dolorem culpa similique itaque debitis excepturi minima nemo, ab dolores magnam ipsam, dignissimos dicta perferendis fugit consectetur natus cupiditate laudantium.</p>
                    </div>
                    <div class="col-8 d-flex flex-column align-items-center justify-content-center col-lg-4 ">
                        <img src="https://picsum.photos/300" class="img-fluid d-none d-lg-block img-service rounded-circle" alt="">
                    </div>  

                </div>
        </section>
    <header>
</x-layout>